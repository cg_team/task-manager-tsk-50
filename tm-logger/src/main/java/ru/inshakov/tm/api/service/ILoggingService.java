package ru.inshakov.tm.api.service;

import ru.inshakov.tm.dto.LoggerDTO;

public interface ILoggingService {

    void writeLog(LoggerDTO message);

}
