package ru.inshakov.tm.listener;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.ILoggingService;
import ru.inshakov.tm.dto.LoggerDTO;
import ru.inshakov.tm.service.LoggingService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

@AllArgsConstructor
public class LogMessageListener implements MessageListener {

    @NotNull
    final ILoggingService loggingService = new LoggingService();

    @Override
    @SneakyThrows
    public void onMessage(Message message) {
        if (message instanceof ObjectMessage) {
            @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
            if (entity instanceof LoggerDTO)
                loggingService.writeLog((LoggerDTO) entity);
        }
    }

}
