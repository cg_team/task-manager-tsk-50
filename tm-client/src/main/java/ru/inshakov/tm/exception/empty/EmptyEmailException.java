package ru.inshakov.tm.exception.empty;

public final class EmptyEmailException extends RuntimeException {

    public EmptyEmailException() {
        super("Error! Email is empty...");
    }

}
