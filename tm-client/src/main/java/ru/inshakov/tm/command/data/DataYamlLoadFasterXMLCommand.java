package ru.inshakov.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

public class DataYamlLoadFasterXMLCommand extends AbstractCommand {

    @Nullable

    public String name() {
        return "data-load-yaml";
    }

    @Nullable

    public String arg() {
        return null;
    }

    @Nullable

    public String description() {
        return "Load data from JSON by FasterXML.";
    }

    public void execute() {
        serviceLocator.getDataEndpoint().loadDataYaml(getSession());
    }

}