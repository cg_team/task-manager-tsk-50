package ru.inshakov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.service.IConnectionService;
import ru.inshakov.tm.model.AbstractGraphEntity;

public abstract class AbstractGraphService<E extends AbstractGraphEntity> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractGraphService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
