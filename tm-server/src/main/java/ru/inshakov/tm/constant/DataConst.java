package ru.inshakov.tm.constant;

public interface DataConst {

    String FILE_BINARY = "./data.bin";

    String FILE_BASE64 = "./data.base64";

    String FILE_JSON_FASTERXML = "./data-fasterxml.json";

    String FILE_JSON_JAXB = "./data-jaxb.json";

    String FILE_XML_FASTERXML = "./data-fasterxml.xml";

    String FILE_XML_JAXB = "./data-jaxb.xml";

    String FILE_YAML_FASTERXML = "./data-fasterxml.yaml";

    String BACKUP_XML = "./backup.xml";

}
