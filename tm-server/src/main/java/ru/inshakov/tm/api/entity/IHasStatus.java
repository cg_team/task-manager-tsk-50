package ru.inshakov.tm.api.entity;

import ru.inshakov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
