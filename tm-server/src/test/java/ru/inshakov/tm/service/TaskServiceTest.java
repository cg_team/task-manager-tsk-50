package ru.inshakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.inshakov.tm.dto.Task;
import ru.inshakov.tm.dto.User;
import ru.inshakov.tm.exception.empty.EmptyIdException;
import ru.inshakov.tm.exception.empty.EmptyIndexException;
import ru.inshakov.tm.exception.empty.EmptyNameException;
import ru.inshakov.tm.service.dto.TaskService;
import ru.inshakov.tm.service.dto.UserService;

import java.ru.inshakov.tm.marker.DBCategory;
import java.util.List;

public class TaskServiceTest {

    @Nullable
    private static TaskService taskService;

    @Nullable
    private static User user;

    @Nullable
    private Task task;

    @BeforeClass
    public static void beforeClass() {
        taskService = new TaskService(new ConnectionService(new PropertyService()));
        @NotNull final UserService userService =
                new UserService(new ConnectionService(new PropertyService()), new PropertyService());
        userService.add("test", "test");
        user = userService.findByLogin("test");
    }

    @Before
    public void before() {
        task = taskService.add(user.getId(), new Task("test_task"));
    }

    @Test
    @Category(DBCategory.class)
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("test_task", task.getName());

        @NotNull final Task taskById = taskService.findById(task.getId());
        Assert.assertNotNull(taskById);
        Assert.assertEquals(task.getId(), taskById.getId());
    }

    @Test
    @Category(DBCategory.class)
    public void findAll() {
        @NotNull final List<Task> tasks = taskService.findAll();
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskService.findAll(user.getId());
        Assert.assertTrue(tasks.size() > 0);
    }

    @Test
    @Category(DBCategory.class)
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskService.findAll("test");
        Assert.assertNotEquals(1, tasks.size());
    }

    @Test
    @Category(DBCategory.class)
    public void findById() {
        @NotNull final Task task = taskService.findById(user.getId(), this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrect() {
        @NotNull final Task task = taskService.findById(user.getId(), "1234");
        Assert.assertNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void findByIdNull() {
        @NotNull final Task task = taskService.findById(user.getId(), null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIdIncorrectUser() {
        @NotNull final Task task = taskService.findById("test", this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void remove() {
        taskService.removeById(task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void testRemoveNull() {
        taskService.removeById(null);
    }

    @Test
    @Category(DBCategory.class)
    public void findByName() {
        @NotNull final Task task = taskService.findByName(user.getId(), "test_task");
        Assert.assertNotNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrect() {
        @NotNull final Task task = taskService.findByName(user.getId(), "1234");
        Assert.assertNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void findByNameNull() {
        @NotNull final Task task = taskService.findByName(user.getId(), null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByNameIncorrectUser() {
        @NotNull final Task task = taskService.findByName("test", this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void findByIndex() {
        @NotNull final Task task = taskService.findByIndex(user.getId(), 1);
        Assert.assertNotNull(task);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void findByIndexNull() {
        @NotNull final Task task = taskService.findByIndex(user.getId(), null);
        Assert.assertNull(task);
    }

    @Test
    @Category(DBCategory.class)
    public void removeById() {
        taskService.removeById(user.getId(), task.getId());
        Assert.assertNull(taskService.findById(task.getId()));
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIdException.class)
    public void removeByIdNull() {
        taskService.removeById(user.getId(), null);
    }

    @Category(DBCategory.class)
    @Test(expected = EmptyIndexException.class)
    public void removeByIndexNull() {
        taskService.removeByIndex(user.getId(), null);
    }


    @Category(DBCategory.class)
    @Test(expected = EmptyNameException.class)
    public void removeByNameNull() {
        taskService.removeByName(user.getId(), null);
    }

}
